/*
 * Copyright (c) 2015. Kirtesh Wani (Selenium By Needs)
 * http://www.seleniumbyneeds.blogspot.com
 */

package com.kirwa.java;

import java.io.File;

/**
 * ****************************
 * Created by Kirtesh Wani on 19-02-2015.
 * for WebDriverCourse
 * *****************************
 */
public class BaseTest {
    //// Wait Method for a class
    void waitForAwhile(long wait) {
        try {
            Thread.sleep(wait*1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    String getPathToResource(String htmlFilename, String simpleName) {
        char sc = File.separatorChar;
        String currentDir = System.getProperty("user.dir");
        String resourcesPath = currentDir + sc + "src" + sc + "resources"+ sc + simpleName ;
        String htmlPath = resourcesPath + sc + htmlFilename;
        return htmlPath;
    }
}
