/*
 * Copyright (c) 2015. Kirtesh Wani (Selenium By Needs)
 * http://www.SeleniumByNeeds.blogspot.com
 */

package com.kirwa.java;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class E1 extends BaseTest {
	/*
	In this exercise you will see how we can locate the web element & do basic operations like
	-- Typing in text box
	-- clicking Button / Links
	-- Locating Element by ID
	-- Locating Element by Link Text (Links with mentioned text)
	We have used Thread.sleep methods to synchronise the page load time
	 */

    @Test
	public void  testForValidLogin(){
		WebDriver driver = new FirefoxDriver();
		String htmlPath = this.getPathToResource("index.html", this.getClass().getSimpleName());
		driver.get(htmlPath);
		driver.findElement(By.id("user")).sendKeys("admin");
		driver.findElement(By.id("password")).sendKeys("pa$$w0rd");
		driver.findElement(By.id("login")).click();
		//Explicit Wait
        this.waitForAwhile(3);
		Assert.assertTrue(driver.findElements(By.linkText("Logout")).size()>0);
        driver.quit();
	}

	@Test
	public void testForInvalidLogin(){
		WebDriver driver = new FirefoxDriver();
		String htmlPath = this.getPathToResource("index.html", this.getClass().getSimpleName());
		driver.get(htmlPath);
		driver.findElement(By.id("user")).sendKeys("admin1");
		driver.findElement(By.id("password")).sendKeys("pa$$w0rd");
		driver.findElement(By.id("login")).click();
		this.waitForAwhile(3);
		Assert.assertFalse(driver.findElements(By.linkText("Logout")).size()>0);
		driver.quit();
	}

    @Test
    public void checkForLogOutFunctionality(){
        WebDriver driver = new FirefoxDriver();
        String htmlPath = this.getPathToResource("index.html", this.getClass().getSimpleName());
        driver.get(htmlPath);
        driver.findElement(By.id("user")).sendKeys("admin");
        driver.findElement(By.id("password")).sendKeys("pa$$w0rd");
        driver.findElement(By.id("login")).click();
        this.waitForAwhile(3);
        driver.findElement(By.linkText("Logout")).click();
        Assert.assertFalse(driver.findElements(By.id("login")).size()>0);
        driver.quit();
    }

}
