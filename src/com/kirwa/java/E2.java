/*
 * Copyright (c) 2015. Kirtesh Wani (Selenium By Needs)
 * http://www.seleniumbyneeds.blogspot.com
 */

package com.kirwa.java;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/**
 * ****************************
 * Created by Kirtesh Wani on 19-02-2015.
 * for WebDriverCourse
 * *****************************
 * Now we have a case where we do not know how much time
 * required to do the login or we can say load the dashboard page,
 * it leads to fail in test due to synchronisation problem
 * In such cases we use Implicit Waits
 * Please check the test
 *
 */
public class E2 extends BaseTest{
    @Test
    public void  testForValidLogin(){
        WebDriver driver = new FirefoxDriver();
        /* Here we are giving longest possible wait */
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        String htmlPath = this.getPathToResource("index.html", this.getClass().getSimpleName());
        driver.get(htmlPath);
        driver.findElement(By.id("user")).sendKeys("admin");
        driver.findElement(By.id("password")).sendKeys("pa$$w0rd");
        driver.findElement(By.id("login")).click();
        //Explicit Wait
        this.waitForAwhile(3);
        Assert.assertTrue(driver.findElements(By.linkText("Logout")).size() > 0);
        driver.quit();
    }

    /**********************
    Many time we are also not sure about the Max wait time.
    Take Example like Fetching Bus list from two Station
    in such case we can use below waits
    ***********************/

    @Test
    public void testForWaitTillLoadingImageVisible(){
        WebDriver driver = new FirefoxDriver();
        String htmlPath = this.getPathToResource("index.html", this.getClass().getSimpleName());
        driver.get(htmlPath);
        driver.findElement(By.id("user")).sendKeys("admin");
        driver.findElement(By.id("password")).sendKeys("pa$$w0rd");
        driver.findElement(By.id("login")).click();
        // Here we are putting vey big number for this case only
        WebDriverWait wait = new WebDriverWait(driver,500);
       // Wait until the Overlay has bee disappear ov invisible
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("overlay")));
        this.waitForAwhile(3);
        Assert.assertTrue(driver.findElements(By.linkText("Logout")).size() > 0);
        driver.quit();
    }

}
