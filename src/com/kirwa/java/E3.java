package com.kirwa.java;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by KIRTESH WANI on 2/26/15.
 */
public class E3 extends BaseTest {
    WebDriver driver;
    @Test
    public void testToSelectFirstInRattingForFirstRecord(){
        driver = new FirefoxDriver();
        driver.get(getPathToResource("index.html", "e3"));
        WebElement normSelect = driver.findElement(By.xpath("//select"));
        Select customSelect =  new Select(normSelect);
        customSelect.selectByVisibleText("first");

    }
    @Test
    public void testToSelectPlus1RatingForEachRecord()
    {
        driver = new FirefoxDriver();
        driver.get(getPathToResource("index.html", "e3"));
        List<WebElement> selects=driver.findElements(By.xpath("//select"));
        for (WebElement select : selects) {
            Select aselect = new Select(select);
            int selectedIndex = Integer.parseInt(aselect.getFirstSelectedOption().getAttribute("Value"));
            if(selectedIndex<4){
                aselect.selectByValue(++selectedIndex+"");
            }
        }
    }
}

