package com.kirwa.java;

import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * Created by KIRTESH WANI on 2/27/15.
 */
public class E4 extends BaseTest{
    WebDriver driver;
    @Test
    public void testForChromeDriver(){
        System.setProperty("webdriver.chrome.driver",getPathToResource("chromedriver.exe","common"));
        driver= new ChromeDriver();
        driver.get(getPathToResource("index.html","e3"));
    }

}
